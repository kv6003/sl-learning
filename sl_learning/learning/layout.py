from tkinter import ttk

from sl_learning.base.base import Layout
from sl_learning.base.utils import register_layout
from sl_learning.learning.suite import Suite
from sl_learning.settings.settings import settings


@register_layout
class LearningLayout(Layout):
    LABEL = "learning"

    def __init__(self, master):
        super().__init__(master)

    def grid(self, cnf={}, **kw):
        suite = Suite(self.master, settings.active_sli())
        suite.run()

        quit_btn = ttk.Button(
            self.master,
            text="Back to Menu",
            command=lambda: self.quit_btn_command(suite.video_window),
        )
        quit_btn.grid(row=3, column=2, pady=(0, 20))
        super().grid(cnf, **kw)

    def quit_btn_command(self, video_window):
        video_window.close()
        from sl_learning.base.menu import MenuLayout
        self.switch(MenuLayout.LABEL)
