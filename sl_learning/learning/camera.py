import threading
import time
from typing import Optional, Type

import cv2.cv2 as cv2

from sl_learning.base.limiter import FrequencyLimiter
from sl_learning.sli.base import SignLanguageProcessor


class ThreadCamera:
    def __init__(
        self, frame_processor: Type[Optional[SignLanguageProcessor]] = None
    ):
        self.fps_limit = 60
        self.fps = 0
        self.capture = cv2.VideoCapture(0)
        self.capture.set(cv2.CAP_PROP_BUFFERSIZE, 2)
        self.frame_processor = frame_processor
        self.thread_flag = False

    def start_recording(self):
        thread = threading.Thread(target=self._update_frame, args=())
        thread.daemon = True
        self.thread_flag = True
        thread.start()
        self._load_frame()

    def stop_recording(self):
        self.thread_flag = False
        time.sleep(1)
        self.capture.release()

    def get_fps(self):
        return self.fps

    def get_frame(self):
        return self.frame

    def _process_frame(self):
        while True:
            self.processed_frame = self.frame_processor.run(self.frame)

    def _update_frame(self):
        while self.thread_flag and self.capture.isOpened():
            with FrequencyLimiter(self.fps_limit) as fl:
                status, frame = self.capture.read()
                frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                frame = cv2.flip(frame, 1)
                # frame.flags.writeable = False
                if status and self.frame_processor:
                    frame = self.frame_processor.process(frame)
            self.fps = fl.measured_hz
            self.frame = frame

    def _load_frame(self):
        """ Wait for the thread to load first frame """
        while True:
            time.sleep(0.01)
            if hasattr(self, "frame"):
                return

    def __enter__(self):
        self.start_recording()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.stop_recording()

    def __del__(self):
        if self.capture.isOpened():
            self.stop_recording()
