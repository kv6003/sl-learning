import enum
from dataclasses import dataclass, field
from typing import List, Optional


class RoundDataStatus(enum.Enum):
    UNPROCESSED = 0
    CORRECT = 1
    CORRECTED = 2
    WRONG = 3


@dataclass
class RoundData:
    word_class: str
    final_predictions: List[str] = field(default_factory=lambda: [])

    def add(self, prediction):
        self.final_predictions.append(prediction)

    @property
    def number_of_attempts(self):
        return len(self.final_predictions)

    @property
    def status(self):
        if not self.final_predictions:
            return RoundDataStatus.UNPROCESSED
        elif self.final_predictions[0] == self.word_class:
            return RoundDataStatus.CORRECT
        elif self.word_class in self.final_predictions:
            return RoundDataStatus.CORRECTED
        return RoundDataStatus.WRONG

    @property
    def incorrect_predictions(self):
        unique_fp = set(self.final_predictions)
        return [e for e in unique_fp if e != self.word_class]


class RoundDataList:

    def __init__(self, wc_list: Optional[List[str]] = None):
        self.data = []
        if wc_list:
            self.data = [RoundData(wc) for wc in wc_list]

    def append(self, item: RoundData) -> None:
        assert type(item) == RoundData
        self.data.append(item)

    def __len__(self):
        return len(self.data)

    def __getitem__(self, i):
        assert type(i) == int
        return self.data[i]

    @property
    def correct_data_points(self):
        correct_statuses = [RoundDataStatus.CORRECT, RoundDataStatus.CORRECTED]
        correct = 0
        for rs in self.data:
            if rs.status in correct_statuses:
                correct += 1
        return correct
