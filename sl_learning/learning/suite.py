from enum import Enum
from tkinter import ttk, LEFT
import tkinter as tk

from sl_learning.learning.exceptions import UnknownStageException
from sl_learning.learning.round_data import RoundDataList, RoundDataStatus
from sl_learning.learning.video_window import VideoWindow


class SuiteStages(Enum):
    START = 0
    GESTURING = 1
    MID_STEP = 2
    RESULTS = 3


class Suite:
    _STAGE_ORDER = [
        SuiteStages.START,
        SuiteStages.GESTURING,  # menu_btn
        SuiteStages.MID_STEP,   # menu_btn, repeat_btn, next_btn
        SuiteStages.RESULTS
    ]
    _BUTTONS = ["menu_btn", "repeat_btn", "next_btn", "results_btn"]

    def __init__(self, master, sli):
        self.master = master
        self.sli = sli
        self.stage = SuiteStages.START
        self.stage_series = RoundDataList(sli.generate_learning_words())
        self.stage_series_index = 0

        self.guide_label = ttk.Label(text="", font=("lucida", 48, "bold"))
        self.feedback_label = ttk.Label(text="", font=("lucida", 48, "bold"))
        self.repeat_btn = ttk.Button(text="Repeat", width=7, command=lambda: self.__repeat_command())
        self.step_btn = ttk.Button(text="Next", width=7, command=lambda: self.__step_command(), default="active")

        self.video_window = VideoWindow(self.master, sli)

    def run(self):
        def start_btn_command():
            for widget in self.master.winfo_children():
                if not (widget.widgetName == "ttk::button" and widget["text"] == "Back to Menu"):
                    widget.grid_forget()
            self._display_learning_layout()
            self.master.eval('tk::PlaceWindow . center')
            self.next_stage()
        start_btn = ttk.Button(text="Start", command=lambda: start_btn_command(), default="active")
        msg = "After clicking the 'Next' button the learning application will be launched, if you are not sure\n" \
              "what to do upon launch, please read the guide which can be found on the main menu, where\n" \
              "everything is clearly explained."
        disclaimer_lbl = tk.Label(text=msg)
        disclaimer_lbl.grid(row=0, column=1, columnspan=5, padx=40, pady=30)
        start_btn.grid(row=3, column=3, pady=(0, 20), sticky="e")

    @property
    def active_round(self):
        return self.stage_series[self.stage_series_index]

    @property
    def active_text(self):
        return self.stage_series[self.stage_series_index].word_class

    def _run_learning_hub(self):
        match self.stage:
            case SuiteStages.START:
                pass
            case SuiteStages.GESTURING:
                if self.repeat_btn.winfo_exists():
                    self.repeat_btn.grid_forget()
                if self.step_btn.winfo_exists():
                    self.step_btn.grid_forget()
                self._count_down()
                self._gesturing_stage_loop()
            case SuiteStages.MID_STEP:
                self._mid_step_stage_action()
            case SuiteStages.RESULTS:
                self._results_stage_action()
            case _:
                raise UnknownStageException(f"Unknown suite stage '{self.stage}'")

    def _display_learning_layout(self):
        # display video window
        self.video_window.grid(row=0, column=0, columnspan=5)
        # display guide label
        self.guide_label.config(text=self.active_text)
        self.guide_label.grid(row=1, column=1, columnspan=3, pady=(0, 20))
        # display feedback label
        self.feedback_label.grid(row=2, column=1, columnspan=3, pady=(0, 20))
        self._run_learning_hub()

    def _count_down(self, i=3):
        self.feedback_label.config(foreground="gray")
        if i == 0:
            self.feedback_label.config(text="Go!")
            self.sli.gesturing_flag = True
        else:
            self.feedback_label.config(text=i)
        if i >= 0:
            self.master.after(700, self._count_down, i - 1)
        else:
            self.sli.start()
            self.feedback_label.config(text="")

    def _gesturing_stage_loop(self):
        if prediction := self.sli.yield_round_prediction():
            self.feedback_label.config(text=prediction)
            match = prediction.lower() == self.active_text.lower()
            self.feedback_label.config(foreground="green" if match else "red")
            self.sli.pause()
            self.active_round.add(prediction)
            self.next_stage()
            return
        else:
            self.master.after(200, self._gesturing_stage_loop)

    def _mid_step_stage_action(self):
        self.repeat_btn.grid(row=3, column=1, pady=(0, 20))
        self.step_btn.grid(row=3, column=3, pady=(0, 20))
        self.step_btn.config(text="Results" if self.is_last_step() else "Next")

    def _results_stage_action(self):
        self.video_window.close()
        quit_btn = None
        for widget in self.master.winfo_children():
            if widget.widgetName == "ttk::button" and widget["text"] == "Back to Menu":
                quit_btn = widget
                quit_btn.grid_forget()
            else:
                widget.destroy()
        text = f"Correct: {self.stage_series.correct_data_points}/{len(self.stage_series)}"
        correctness_label = ttk.Label(text=text, font=("lucida", 48, "bold"))
        correctness_label.grid(row=0, column=0, columnspan=3, pady=(5, 20))

        column_h_font = ("lucida", 28, "bold")
        tk.Label(text="Sign", font=column_h_font).grid(row=1, column=0, padx=(50, 10))
        tk.Label(text="Number of\nattempts", font=column_h_font).grid(row=1, column=1, padx=(0, 10))
        tk.Label(text="Mistaken\nsigns", font=column_h_font).grid(row=1, column=2, padx=(0, 50))

        fg = {
            RoundDataStatus.CORRECT: "green",
            RoundDataStatus.CORRECTED: "orange",
            RoundDataStatus.WRONG: "red"
        }
        table_font = ("lucida", 24)
        word_column_styling = {"font": table_font, "justify": LEFT, "anchor": "w"}
        for i in range(len(self.stage_series)):  # Rows
            stage_data = self.stage_series[i]
            word_class = tk.Label(text=stage_data.word_class, **word_column_styling, foreground=fg.get(stage_data.status))
            no_of_trials = tk.Label(text=stage_data.number_of_attempts, font=table_font, justify=LEFT, width=10)
            mistakes = tk.Label(text=", ".join(stage_data.incorrect_predictions) or "-", **word_column_styling)

            row = i + 2
            word_class.grid(row=row, column=0, sticky="nesw", padx=(50, 10))
            no_of_trials.grid(row=row, column=1, padx=(0, 10))
            mistakes.grid(row=row, column=2, sticky="nesw", padx=(0, 50))
            self.master.update()

        quit_btn.config(text="Back to Menu")
        quit_btn.grid(row=len(self.stage_series) + 3, column=1, pady=(30, 20))
        self.master.eval('tk::PlaceWindow . center')

    def is_last_step(self) -> bool:
        return self.stage_series_index >= len(self.stage_series) - 1

    def next_stage(self):
        stage_i = self._STAGE_ORDER.index(self.stage)
        self.stage = self._STAGE_ORDER[stage_i + 1]
        self._run_learning_hub()

    def previous_stage(self):
        stage_i = self._STAGE_ORDER.index(self.stage)
        self.stage = self._STAGE_ORDER[stage_i - 1]
        self._run_learning_hub()

    def __step_command(self):
        self.feedback_label.config(text="")
        self.sli.reset()
        if self.is_last_step():
            self.next_stage()
        else:
            self.stage_series_index += 1
            self.guide_label.config(text=self.active_text)
            self.previous_stage()

    def __repeat_command(self):
        self.feedback_label.config(text="")
        self.sli.reset()
        self.previous_stage()
