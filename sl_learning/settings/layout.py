import tkinter as tk
from tkinter import ttk

from sl_learning.base.utils import register_layout, switch_layout
from sl_learning.settings.conf import SLI_REGISTRY
from sl_learning.settings.settings import settings


@register_layout
class Settings(tk.Frame):
    LABEL = "settings"

    def grid(self, cnf={}, **kw):
        self._render_settings_heading()
        self._render_active_sli()
        self._render_display_fps()
        self._render_draw_results()

        sli_settings_heading = tk.Label(self.master, text="SLI Settings", font=("lucida", 24))
        sli_settings_heading.grid(row=4, column=0, padx=15, pady=(20, 12), columnspan=2)
        grid_size = self.master.grid_size()
        settings.active_sli.display_sli_settings(self.master, grid_size[1] + 1, grid_size[0])

        from sl_learning.base.menu import MenuLayout
        grid_size = self.master.grid_size()
        back_b = tk.Button(self.master, text="Back to Menu", command=lambda: switch_layout(MenuLayout.LABEL))
        back_b.grid(row=grid_size[1], column=0, padx=15, pady=25, columnspan=grid_size[0])
        super().grid(cnf, **kw)

    def _render_settings_heading(self):
        settings_heading = tk.Label(self.master, text="Settings", font=("lucida", 24))
        settings_heading.grid(row=0, column=0, pady=(20, 12), columnspan=2)

    def _render_active_sli(self):
        def switch_sli():
            setattr(settings, "active_sli", active_sli_val.get())
            for widget in self.master.winfo_children():
                widget.destroy()
            self.grid()

        active_sli_label = tk.Label(self.master, text="Select language: ")
        active_sli_val = tk.StringVar(value=settings.active_sli.LABEL)
        active_sli = ttk.Combobox(
            self.master,
            values=list(SLI_REGISTRY.keys()),
            state="readonly",
            textvariable=active_sli_val
        )
        active_sli.bind("<<ComboboxSelected>>", lambda e: switch_sli())
        active_sli_label.grid(row=1, column=0, padx=15, pady=8, sticky="w")
        active_sli.grid(row=1, column=1, padx=15, pady=8, sticky="w")

    def _render_display_fps(self):
        display_fps_label = tk.Label(self.master, text="Display FPS: ")
        display_fps_val = tk.BooleanVar(value=settings.display_fps, name="display_fps_val")
        display_fps = ttk.Checkbutton(
            self.master,
            onvalue=True,
            offvalue=False,
            variable=display_fps_val,
            command=lambda: setattr(settings, "display_fps", display_fps_val.get())
        )
        display_fps.state(['!alternate'])
        if settings.display_fps:
            display_fps.state(['selected'])
        display_fps_label.grid(row=2, column=0, padx=15, pady=8, sticky="W")
        display_fps.grid(row=2, column=1, padx=15, pady=8, sticky="W")

    def _render_draw_results(self):
        draw_results_label = tk.Label(self.master, text="Draw results: ")
        draw_results_val = tk.BooleanVar(value=settings.draw_results, name="draw_results_val")
        draw_results = ttk.Checkbutton(
            self.master,
            onvalue=True,
            offvalue=False,
            variable=draw_results_val,
            command=lambda: setattr(settings, "draw_results", draw_results_val.get())
        )
        draw_results.state(['!alternate'])
        if settings.draw_results:
            draw_results.state(['selected'])
        draw_results_label.grid(row=3, column=0, padx=15, pady=8, sticky="W")
        draw_results.grid(row=3, column=1, padx=15, pady=8, sticky="W")
