import json
import os
import cv2.cv2 as cv2

from sl_learning.settings.conf import SLI_REGISTRY


class Settings:
    SETTINGS_DIR = os.path.dirname(os.path.realpath(__file__))
    _initialized = False

    def __init__(self):
        with open(os.path.join(self.SETTINGS_DIR, "config.json"), "rb") as f:
            self.base_settings = json.load(f)
        self.sli_settings = dict()
        self.additional_settings = dict()

    @property
    def active_sli(self):
        return SLI_REGISTRY[self.base_settings["active_sli"]]

    def max_fps(self):
        video = cv2.VideoCapture(0)
        settings.max_cam_fps = video.get(cv2.CAP_PROP_FPS)
        video.release()

    def load_sli(self):
        active_sli = self.active_sli
        with open(active_sli.config_dir, "rb") as f:
            self.sli_settings = json.load(f)
        self._initialized = True

    def sync_config(self):
        # load base settings
        with open(os.path.join(self.SETTINGS_DIR, "config.json"), "w") as fp:
            json.dump(self.base_settings, fp)
        # load sli settings
        with open(self.active_sli.config_dir, "w") as fp:
            json.dump(self.sli_settings, fp)

    def __getattr__(self, item):
        # if item == "active_sli":
        #     return SLI_REGISTRY["active_sli"]
        return {
            **self.base_settings,
            **self.sli_settings,
            **self.additional_settings,
            "menu_label": "menu"
        }[item]

    def __setattr__(self, key, value):
        if not self._initialized:
            super().__setattr__(key, value)
            return
        if key == "active_sli":
            for cls in SLI_REGISTRY.values():
                if cls.LABEL == value:
                    self.base_settings["active_sli"] = value
                    self.sync_config()
                    return
            else:
                raise Exception("Unknown sign language implementation selected")
        for settings_type in [self.base_settings, self.sli_settings]:
            for k in settings_type.keys():
                if k == key:
                    if type(k) != type(key):
                        raise Exception(
                            "Value of a configuration setting cannot be changed to different type than initial"
                        )
                    settings_type[key] = value
                    break
        else:
            self.additional_settings[key] = value
        self.sync_config()


settings = Settings()
