import os
from pathlib import Path

PROJECT_ROOT = str(Path(os.path.realpath(__file__)).parent.parent.parent) + "/"
SLI_ROOT = os.path.join(PROJECT_ROOT, "sl_learning", "sli/")
SLI_REGISTRY = dict()
