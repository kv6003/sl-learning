import importlib
from pathlib import Path

from sl_learning.settings.conf import SLI_ROOT, PROJECT_ROOT


def autodiscover_sli():
    for processor_dir in list(Path(SLI_ROOT).glob("**/processor.py")):
        processor_dir = Path(str(processor_dir).replace(PROJECT_ROOT, ""))
        importlib.import_module('.'.join(processor_dir.parts).replace('.py', ''))
