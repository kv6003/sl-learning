import tkinter as tk
from tkinter import ttk

import tkmacosx as tkosx

from sl_learning.base.guide import GuideLayout
from sl_learning.base.utils import register_layout, switch_layout
from sl_learning.learning.layout import LearningLayout
from sl_learning.sandbox.layout import SandboxLayout
from sl_learning.settings.layout import Settings
from sl_learning.settings.settings import settings


@register_layout
class MenuLayout(tk.Frame):
    LABEL = settings.menu_label  # "menu"

    def grid(self, cnf={}, **kw):
        start_btn = tkosx.Button(
            self.master,
            text="Learning",
            command=lambda: switch_layout(LearningLayout.LABEL),
            bg='#2686fb',
            fg='white',
            borderless=0
        )
        sandbox_btn = ttk.Button(
            self.master,
            text="Sandbox",
            command=lambda: switch_layout(SandboxLayout.LABEL)
        )
        guide_bth = ttk.Button(
            self.master,
            text="Guide",
            command=lambda: switch_layout(GuideLayout.LABEL)
        )
        settings_btn = ttk.Button(
            self.master,
            text="Settings",
            command=lambda: switch_layout(Settings.LABEL)
        )
        exit_btn = ttk.Button(self.master, text="Exit", command=self.master.quit)
        language_heading_lbl = ttk.Label(text="Current language:", font=("lucida", 18))
        language_lbl = ttk.Label(text=settings.active_sli.LABEL, font=("lucida", 16))
        language_lbl_tip = ttk.Label(
            text="You can switch language\nin the settings tab",
            foreground="gray"
        )

        start_btn.grid(row=0, column=0, padx=30, pady=30)
        sandbox_btn.grid(row=1, column=0, padx=30, pady=(0, 30))
        guide_bth.grid(row=2, column=0, padx=30, pady=(0, 30))
        settings_btn.grid(row=3, column=0, padx=30, pady=(0, 30))
        exit_btn.grid(row=4, column=0, padx=30, pady=(0, 30))

        language_heading_lbl.grid(row=0, column=1, padx=30, pady=(5, 0))
        language_lbl.grid(row=1, column=1, padx=30, sticky="n")
        language_lbl_tip.grid(row=4, column=1, sticky="n")
        super().grid(cnf, **kw)
