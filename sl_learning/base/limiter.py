import time


class FrequencyLimiter:
    def __init__(self, hz):
        self.target_hz = hz
        self.start = None
        self.measured_hz = None

    def __enter__(self):
        self.start = time.time()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        exec_time = time.time() - self.start
        sleep_time = (1 / self.target_hz) - exec_time
        time.sleep(sleep_time if sleep_time >= 0 else 0)
        end_time = time.time()
        self.measured_hz = float(format(1 / (end_time - self.start), ".1f"))
