from sl_learning.settings.settings import settings

LAYOUT_REGISTRY = dict()


def register_layout(cls):
    LAYOUT_REGISTRY[cls.LABEL] = cls
    return cls


class classproperty:    # NOQA - lowercase for a purpose, it is meant to be a wrapper
    """
    Decorator that converts a method with a single cls argument into a property
    that can be accessed directly from the class.
    """
    def __init__(self, method=None):
        self.method = method

    def __get__(self, instance, cls=None):
        return self.method(cls)

    # def getter(self, method):
    #     self.method = method
    #     return self


def switch_layout(switch_label, **kwargs):
    # Exit layout
    for widget in settings.root.winfo_children():
        # widget.pack_forget()
        widget.destroy()
    # Display new layout
    layout_cls = LAYOUT_REGISTRY[switch_label]
    new_layout = layout_cls(settings.root, **kwargs)
    new_layout.grid(row=0, column=0)
    settings.root.eval('tk::PlaceWindow . center')
