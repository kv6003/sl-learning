import tkinter as tk
from tkinter import ttk

from sl_learning.base.utils import register_layout, switch_layout
from sl_learning.settings.settings import settings


@register_layout
class GuideLayout(tk.Frame):
    LABEL = "guide"

    def grid(self, cnf={}, **kw):
        info_txt = "After clicking the start button, a string of text on the bottom of the window will appear.\n" \
                   "Please make gestures to express the displayed the displayed text after the countdown ends."
        info_lbl = tk.Label(text=info_txt)

        quit_btn = ttk.Button(
            self.master,
            text="Back to Menu",
            command=lambda: switch_layout(settings.menu_label),
        )

        info_lbl.grid(row=0, column=0, padx=40, pady=40)
        quit_btn.grid(row=1, column=0, pady=(0, 40))
        super().grid(cnf, **kw)
