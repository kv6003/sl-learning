import tkinter as tk
from typing import List

from sl_learning.base.utils import LAYOUT_REGISTRY


class Layout(tk.Frame):
    LABEL: str

    def __init__(self, master=None, cnf={}, **kw):
        super().__init__(master, cnf, **kw)
        self.master = master

    def display(self):
        raise NotImplementedError

    def switch(self, switch_label):
        # Exit layout
        for widget in self.master.winfo_children():
            # widget.pack_forget()
            widget.destroy()
        # Display new layout
        layout_cls = LAYOUT_REGISTRY[switch_label]
        new_layout = layout_cls(self.master)
        new_layout.grid()
        self.master.eval('tk::PlaceWindow . center')
