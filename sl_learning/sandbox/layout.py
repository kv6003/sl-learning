import tkinter as tk
from tkinter import ttk

from sl_learning.base.utils import register_layout, switch_layout
from sl_learning.sandbox.sandbox import Sandbox
from sl_learning.settings.settings import settings


@register_layout
class SandboxLayout(tk.Frame):
    LABEL = "sandbox"

    def __init__(self, master):
        super().__init__(master)

    def grid(self, cnf={}, **kw):
        sb = Sandbox(self.master)
        sb.grid(row=0, column=0)

        quit_btn = ttk.Button(
            self.master,
            text="Back to Menu",
            command=lambda: self.quit_btn_command(sb),
        )
        quit_btn.grid(row=1, column=0, pady=(0, 20))
        super().grid(cnf, **kw)

    def quit_btn_command(self, sb):
        sb.stop()
        switch_layout(settings.menu_label)
