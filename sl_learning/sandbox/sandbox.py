import tkinter as tk

from sl_learning.learning.video_window import VideoWindow
from sl_learning.settings.settings import settings


class Sandbox(tk.Frame):
    CANVAS_HEIGHT = 200
    BIN_HEIGHT = 180
    BIN_WIDTH = 60
    BIN_SPACE = 30

    def __init__(self, master=None, cnf={}, **kw):
        super(Sandbox, self).__init__(master, cnf, **kw)
        self.sli = settings.active_sli()
        self.video_window = VideoWindow(self, self.sli)
        self.execution_flag = True

    def grid(self, cnf={}, **kw):
        self.video_window.grid(row=0, column=0, columnspan=5)
        bin_count = len(self.sli.WORD_CLASSES)
        width = (self.BIN_SPACE * (bin_count + 1)) + (self.BIN_WIDTH * bin_count)
        c = tk.Canvas(self, width=width, height=self.CANVAS_HEIGHT)
        for i, wc in enumerate(self.sli.WORD_CLASSES):
            x1 = self.BIN_SPACE + (self.BIN_SPACE * i) + (self.BIN_WIDTH * i) + self.BIN_WIDTH / 2
            c.create_text(x1, 190, text=wc)
        c.create_line(0, self.BIN_HEIGHT, width, self.BIN_HEIGHT)
        c.grid(row=1, column=0, columnspan=5)
        self._plot_weights(c)
        super().grid(cnf, **kw)

    def stop(self):
        self.execution_flag = False
        self.video_window.close()

    def _plot_weights(self, canvas, bins=None):
        if not self.execution_flag:
            return
        new_bins = []
        if predictions := self.sli.get_frame_prediction():
            for i, (k, v) in enumerate(predictions.items()):
                y1 = self.BIN_HEIGHT - (v * self.BIN_HEIGHT)
                x1 = self.BIN_SPACE + (self.BIN_SPACE * i) + (self.BIN_WIDTH * i)
                x2 = x1 + self.BIN_WIDTH
                b = canvas.create_rectangle(x1, y1, x2, self.BIN_HEIGHT, fill="red")
                new_bins.append(b)
            if bins:
                for b in bins:
                    canvas.delete(b)
            canvas.grid(row=1, column=0, columnspan=5)
        self.after(100, self._plot_weights, canvas, new_bins)
