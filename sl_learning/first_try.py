import queue
import time
from threading import Thread

import PySimpleGUI as sg
import cv2.cv2 as cv2
import mediapipe as mp

mp_holistic = mp.solutions.holistic  # Holistic model
holistic_model = mp_holistic.Holistic(
    min_detection_confidence=0.5, min_tracking_confidence=0.5
)
stop_thread = False  # controls thread execution


def detect_landmarks(img, model):  # mediapipe_detection
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    img.flags.writeable = False
    result = model.process(img)
    img.flags.writeable = True
    img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
    return img, result


def draw_landmarks(img, lmx):
    mp_drawing = mp.solutions.drawing_utils  # Drawing utilities
    mp_drawing.draw_landmarks(img, lmx.face_landmarks, mp_holistic.FACEMESH_CONTOURS)
    mp_drawing.draw_landmarks(img, lmx.pose_landmarks, mp_holistic.POSE_CONNECTIONS)
    mp_drawing.draw_landmarks(
        img, lmx.left_hand_landmarks, mp_holistic.HAND_CONNECTIONS
    )
    mp_drawing.draw_landmarks(
        img, lmx.right_hand_landmarks, mp_holistic.HAND_CONNECTIONS
    )


def start_capture_thread(cap, queue):
    global stop_thread
    # continuously read fames from the camera
    while True:
        _, frame = cap.read()
        queue.put(frame)
        frame, landmarks = detect_landmarks(frame, holistic_model)
        draw_landmarks(frame, landmarks)
        if stop_thread:
            break


def main():
    global stop_thread
    sg.theme("DarkAmber")
    # define the window layout
    layout = [
        [
            sg.Text(
                "Sign language learning",
                size=(40, 1),
                justification="center",
                font="Helvetica 20",
            )
        ],
        [sg.Image(filename="", key="image")],
        [
            sg.RButton("Start", size=(10, 1), font="Any 14"),
            sg.ReadButton("Exit", size=(10, 1), pad=((200, 0), 3), font="Helvetica 14"),
        ],
    ]
    window = sg.Window("Sign language learning", layout, location=(800, 400))

    cap = cv2.VideoCapture(0)
    cap.set(38, 3)
    cap_width = cap.get(cv2.CAP_PROP_FRAME_WIDTH)
    cap_height = cap.get(cv2.CAP_PROP_FRAME_HEIGHT)
    cap_fps = cap.get(cv2.CAP_PROP_FPS)
    print("* Capture width:", cap_width)
    print("* Capture height:", cap_height)
    print("* Capture FPS:", cap_fps)

    frames_queue = queue.Queue(maxsize=0)
    t = Thread(
        target=start_capture_thread, args=(cap, frames_queue), daemon=True
    )  # a deamon thread is killed when the application exits
    t.start()

    started = False
    while True:
        if frames_queue.empty():
            continue

        start_time = time.time()
        event, values = window.read(timeout=1, timeout_key="timeout")
        if (event == sg.WIN_CLOSED) or (event == "Exit"):
            stop_thread = True
            break
        elif event == "Start":
            started = True

        frame = frames_queue.get()
        if started:
            cv2.imshow("webcam", frame)
        print("FPS: ", 1.0 / (time.time() - start_time))
    window.close()


if __name__ == "__main__":
    main()
