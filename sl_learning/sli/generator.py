class TextSeriesGenerator:

    def __init__(self, word_classes_dir):
        with open(word_classes_dir, "r") as f:
            self.classes = f.read().split("\n")

    def generate(self):
        raise NotImplementedError


class BaseTextSeriesGenerator:

    def generate(self):
        pass
