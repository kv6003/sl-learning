import inspect
import os
import tkinter as tk
from pathlib import Path
from typing import Optional, Tuple

import numpy as np

from sl_learning.base.utils import classproperty
from sl_learning.settings.conf import SLI_REGISTRY
from sl_learning.settings.settings import settings


class SignLanguageProcessor:
    LABEL: str  # in settings displayed as one of the implementations
    WORD_CLASSES: list
    PREDICTION_HANDLER: type
    ROUND_FRAME_LIMIT: int
    CONFIG_FILENAME: str = "config.json"

    def __init__(self):
        self.results = []
        self.process_flag = False
        self.prediction_handler = self.PREDICTION_HANDLER()

    def __init_subclass__(cls, **kwargs):
        """ register subclasses in the SLI registry """
        if hasattr(cls, "LABEL"):
            SLI_REGISTRY[cls.LABEL] = cls

    def _process_frame(self, frame) -> Tuple[Optional[str], Optional[dict]]:
        """Perform prediction on frame and return result, and
        return model prediction visualization data if implemented """
        raise NotImplementedError

    def _draw_resuts(self, frame, **kwargs) -> np.ndarray:
        """ based on the input data draws model prediction visualization onto a frame """
        raise NotImplementedError

    def process(self, frame):
        """ Calls method to which propagates frame through model, passes the prediction to the prediction_handler
        and if enabled and implemented draws model prediction visualization """
        if self.process_flag:
            prediction, draw_data = self._process_frame(frame)
            self.prediction_handler.handle(prediction)
            if settings.draw_results:
                frame = self._draw_resuts(frame, **draw_data)
        return frame

    def start(self):
        """ Resumes frame propagation through models and model
        prediction visualization drawing on frames if enabled """
        self.process_flag = True

    def pause(self):
        """ Pauses frame propagation through models and model
        prediction visualization drawing on frames if enabled """
        self.process_flag = False

    def reset(self):
        """ clears a list of gathered predictions """
        self.prediction_handler.reset()

    def generate_learning_words(self) -> list[str]:
        """" Generates and returns a list of words for learning session """
        return self.WORD_CLASSES

    def yield_round_prediction(self) -> Optional[str]:
        """ Returns prediction with the round result after the ROUND_FRAME_LIMIT has been reached """
        ph = self.prediction_handler
        if self.ROUND_FRAME_LIMIT <= ph.prediction_count:
            return ph.final_prediction

    @staticmethod
    def display_sli_settings(master, first_row_no, columnspan):
        """ Displays custom SLI settings at the bottom part of the settings tab """
        sli_settings_heading = tk.Label(master, text="No SLI settings available", foreground="gray")
        sli_settings_heading.grid(row=first_row_no, column=0, padx=15, pady=7, columnspan=columnspan)

    @classproperty
    def sli_root(cls) -> str:
        """ Root directory of the SLI """
        return str(Path(inspect.getfile(cls)).parent) + "/"

    @classproperty
    def config_dir(cls):
        """ Directory where the SLI settings are saved in a persistent state """
        conf_dir = os.path.join(cls.sli_root, cls.CONFIG_FILENAME)
        if os.path.isfile(conf_dir):
            return conf_dir
