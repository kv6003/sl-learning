import itertools
import logging
import os
import tkinter as tk
from collections import deque

import cv2.cv2 as cv2
import mediapipe as mp
import numpy as np
from tensorflow import keras

from sl_learning.settings.settings import settings
from sl_learning.sli.asl_holistic.exceptions import OutOfFrameException
from sl_learning.sli.asl_holistic.utils import get_3d_angle, get_2d_distance
from sl_learning.sli.base import SignLanguageProcessor
from sl_learning.sli.prediction_handlers import SequencePredictionHandler

logger = logging.getLogger(__name__)


class HolisticModelProcessor(SignLanguageProcessor):
    LABEL = "Czech (CSE)"
    WORD_CLASSES = [
        'ahoj',
        'ano',
        'babicka',
        'bratr',
        'ctvrtek',
        'dcera',
        'deda',
        'dekuji',
        'den',
        'dite',
        'dnes',
        'dobrou_noc',
        'dobry',
        'dobry_den',
        'jak_se_mas',
        'kocka',
        'mama',
        'muz',
        'nashledanou',
        'ne',
        'nedele',
        'omlouvam_se',
        'patek',
        'pes',
        'pondeli',
        'prosim',
        'rok',
        'sestra',
        'sobota',
        'spatny',
        'streda',
        'syn',
        'tata',
        'utery',
        'vcera',
        'zena',
        'zitra'
    ]
    ROUND_FRAME_LIMIT = 60
    PREDICTION_HANDLER = SequencePredictionHandler
    CONFIG_DIR = os.path.join(
        os.path.dirname(os.path.realpath(__file__)), "config.json"
    )

    def __init__(self):
        super().__init__()
        self.holistic_model = mp.solutions.holistic.Holistic(
            min_detection_confidence=settings.min_detection_confidence,
            min_tracking_confidence=settings.min_tracking_confidence,
        )
        self.model = keras.models.load_model(self.sli_root + "model.h5")
        self.sequence = []
        self.frame_prediction = None

    def _process_frame(self, frame):
        result = None
        landmarks = self._detect_landmarks(frame)
        if self.process_flag:
            self.sequence.append(landmarks)
        if len(self.sequence) == 90:
            # input (1, 90, 25)
            nn_input = self._get_nn_input()
            prediction = self.model.predict(nn_input)[0]
            result = self.get_frame_result(prediction)
            self.sequence = []
            print(result)
        return result, {"landmarks": landmarks}

    def _get_nn_input(self):
        x_input = []
        last_lhand_lmx = (None, None)
        last_rhand_lmx = (None, None)
        last_pose_lmx = (None, None)

        def fill_landmarks(lmx, lmx_group_name, frame_i, last_present_lmx, last_present_frame_i):
            lmx_group = getattr(lmx, lmx_group_name)
            if (last_present_lmx is None) and lmx_group and i > 0:  # fill initial missing
                for partial_lmx in self.sequence[0:frame_i]:
                    setattr(partial_lmx, lmx_group_name, lmx_group)
            missing_frames_count = (frame_i - last_present_frame_i) - 1 if last_present_frame_i else None
            if lmx_group and missing_frames_count == 1:
                setattr(self.sequence[frame_i - 1], lmx_group_name, lmx_group)
            elif lmx_group and missing_frames_count:
                mid_cap = int(last_present_frame_i + 1 + (missing_frames_count / 2))
                for partial_lmx in self.sequence[last_present_frame_i:mid_cap]:
                    setattr(partial_lmx, lmx_group_name, last_present_lmx)
                for partial_lmx in self.sequence[mid_cap:frame_i]:
                    setattr(partial_lmx, lmx_group_name, lmx_group)
            elif (lmx_group is None) and missing_frames_count == 1 and frame_i == 89:
                setattr(self.sequence[frame_i - 1], lmx_group_name, last_present_lmx)
            elif (lmx_group is None) and missing_frames_count and frame_i == 89:
                for partial_lmx in self.sequence[last_present_frame_i + 1:90]:
                    setattr(partial_lmx, lmx_group_name, last_present_lmx)

        for i, landmarks in enumerate(self.sequence):
            lh_lmx = landmarks.left_hand_landmarks
            fill_landmarks(landmarks, "left_hand_landmarks", i, *last_lhand_lmx)
            if lh_lmx:
                last_lhand_lmx = (lh_lmx, i)

            rh_lmx = landmarks.right_hand_landmarks
            fill_landmarks(landmarks, "right_hand_landmarks", i, *last_rhand_lmx)
            if rh_lmx:
                last_rhand_lmx = (rh_lmx, i)

            pose_lmx = landmarks.pose_landmarks
            fill_landmarks(landmarks, "pose_landmarks", i, *last_pose_lmx)
            if pose_lmx:
                last_pose_lmx = (pose_lmx, i)

        for frame_lmx in self.sequence:
            hand_attributes = self._get_hand_attributes(frame_lmx)
            pose_attributes = self._get_pose_attributes(frame_lmx)
            # finger angles (20x); pose wrist distance; lhand y; rhand y; lshoulder angle; rshoulder angle
            # hands 20x; pose 1x; hand 2x; pose 2x
            try:
                frame_attributes = [
                    hand_attributes[0:20],
                    pose_attributes[0:1],
                    hand_attributes[20:22],
                    pose_attributes[1:3]
                ]
            except TypeError:
                raise OutOfFrameException("User is out of the frame bounds")
            x_input.append(list(itertools.chain(*frame_attributes)))
        return np.asarray([x_input])

    def _get_hand_attributes(self, frame_lmx):
        if (frame_lmx.left_hand_landmarks is None) or (frame_lmx.right_hand_landmarks is None):
            return None
        frame_features = []
        hands = {
            "landmarks": [frame_lmx.left_hand_landmarks, frame_lmx.right_hand_landmarks],
            "inputs": [
                [0, 2, 4, lambda angle: int((angle - 30) / 1.5)],  # thumb
                [0, 5, 8, lambda angle: int(angle / 1.8)],  # index (palm angle)
                [0, 9, 12, lambda angle: int(angle / 1.8)],  # middle (palm angle)
                [0, 13, 16, lambda angle: int(angle / 1.8)],  # ring (palm angle)
                [0, 17, 20, lambda angle: int(angle / 1.8)],  # pinky (palm angle)
                [2, 3, 4, lambda angle: int((angle - 60 if angle > 60 else 0) / 1.2)],  # thumb (finger angle)
                [5, 6, 8, lambda angle: int(angle / 1.8)],  # index (finger angle)
                [9, 10, 12, lambda angle: int(angle / 1.8)],  # middle
                [13, 14, 16, lambda angle: int(angle / 1.8)],  # ring
                [17, 18, 20, lambda angle: int(angle / 1.8)]  # pinky
            ]
        }
        for lmx_group in hands["landmarks"]:  # HAND ANGLES
            for input_values in hands["inputs"]:
                lmx = lmx_group.landmark
                result = get_3d_angle(lmx[input_values[0]], lmx[input_values[1]], lmx[input_values[2]])
                frame_features.append(input_values[3](result))

        lh_y = int(frame_lmx.right_hand_landmarks.landmark[8].y * 100)  # LEFT HAND Y COORDINATE
        lh_y = lh_y if lh_y >= 0 else 0
        frame_features.append(lh_y if lh_y <= 100 else 100)

        rh_y = int(frame_lmx.left_hand_landmarks.landmark[8].y * 100)  # RIGHT HAND Y COORDINATE
        rh_y = rh_y if rh_y >= 0 else 0
        frame_features.append(rh_y if rh_y <= 100 else 100)
        return frame_features

    def _get_pose_attributes(self, frame_lmx):
        if frame_lmx.pose_landmarks is None:
            return None
        frame_features = []
        wrist_distance = int(get_2d_distance(  # WRIST DISTANCE
            frame_lmx.pose_landmarks.landmark[15],
            frame_lmx.pose_landmarks.landmark[16]
        ) / np.sqrt(2) * 100)
        frame_features.append(wrist_distance)

        p_lmx = frame_lmx.pose_landmarks.landmark
        l_shoulder_angle = int(get_3d_angle(p_lmx[0], p_lmx[11], p_lmx[13]) / 1.8)  # LEFT SHOULDER ANGLE
        frame_features.append(l_shoulder_angle)

        r_shoulder_angle = int(get_3d_angle(p_lmx[14], p_lmx[12], p_lmx[0]) / 1.8)  # RIGHT SHOULDER ANGLE
        frame_features.append(r_shoulder_angle)
        return frame_features

    def _detect_landmarks(self, frame):
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        frame.flags.writeable = False
        landmarks = self.holistic_model.process(frame)
        return landmarks

    def get_frame_result(self, prediction):
        return self.WORD_CLASSES[np.argmax(prediction)]

    def get_frame_prediction(self):
        if self.frame_prediction is not None and self.frame_prediction.any():
            return {self.WORD_CLASSES[i]: self.frame_prediction[i] for i in range(self.frame_prediction.size)}

    def _draw_resuts(self, frame, **kwargs):
        """Draw landmarks onto a frame"""
        lmx = kwargs["landmarks"]
        mp_holistic = mp.solutions.holistic
        mp_draw = mp.solutions.drawing_utils
        mp_draw.draw_landmarks(frame, lmx.face_landmarks, mp_holistic.FACEMESH_CONTOURS)
        mp_draw.draw_landmarks(frame, lmx.pose_landmarks, mp_holistic.POSE_CONNECTIONS)
        mp_draw.draw_landmarks(frame, lmx.left_hand_landmarks, mp_holistic.HAND_CONNECTIONS)
        mp_draw.draw_landmarks(frame, lmx.right_hand_landmarks, mp_holistic.HAND_CONNECTIONS)
        return frame

    @staticmethod
    def display_sli_settings(master, first_row_no, columnspan):
        mdc_label = tk.Label(master, text="Min. detection confidence", padx=10, pady=8)
        mdc_label.grid(row=first_row_no, column=0)
        mdc_scale_var = tk.DoubleVar(value=settings.min_detection_confidence)
        mdc_scale = tk.Scale(master, variable=mdc_scale_var, from_=0, to=1, resolution=0.01, orient=tk.HORIZONTAL,
                             command=lambda x: setattr(settings, "min_detection_confidence", mdc_scale_var.get()))
        mdc_scale.grid(row=first_row_no, column=1)

        mtc_label = tk.Label(master, text="Min. tracking confidence", padx=10, pady=8)
        mtc_label.grid(row=first_row_no+1, column=0)
        mtc_scale_var = tk.DoubleVar(value=settings.min_tracking_confidence)
        mtc_scale = tk.Scale(master, variable=mtc_scale_var, from_=0, to=1, resolution=0.01, orient=tk.HORIZONTAL,
                             command=lambda x: setattr(settings, "min_detection_confidence", mtc_scale_var.get()))
        mtc_scale.grid(row=first_row_no+1, column=1)

    def generate_learning_words(self) -> list[str]:
        return self.WORD_CLASSES[0:2]

    def reset(self):
        super().reset()
        self.sequence = []

    def yield_round_prediction(self):
        """ Returns prediction with the round result after the ROUND_FRAME_LIMIT has been reached """
        ph = self.prediction_handler
        return ph.final_prediction
