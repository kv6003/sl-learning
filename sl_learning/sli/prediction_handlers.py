from collections import Counter


class PredictionHandler:
    def __init__(self):
        self.predictions = []

    def handle(self, prediction):
        self.predictions.append(prediction)

    def reset(self):
        self.predictions = []

    @property
    def final_prediction(self):
        raise NotImplementedError

    @property
    def prediction_count(self):
        return len(self.predictions)


class StaticPredictionHandler(PredictionHandler):
    """
    A prediction is made with each frame, list of all predictions is then
    processed to determine the final prediction
    """

    @property
    def final_prediction(self):
        cut_index = int(len(self.predictions) / 3)
        filtered_predictions = self.predictions[cut_index:]
        p_counter = Counter(filtered_predictions)
        return p_counter.most_common(1)[0][0]


class ContinuousPredictionHandler(PredictionHandler):
    """ Continuous input of  """

    @property
    def final_prediction(self):
        filtered_predictions = [x for x in self.predictions if x is not None]
        p_counter = Counter(filtered_predictions)
        return p_counter.most_common(1)[0][0]


class SequencePredictionHandler(PredictionHandler):
    """ Requires a sequence of inputs to build up in order to make a prediction"""

    @property
    def final_prediction(self):
        filtered_predictions = [x for x in self.predictions if x is not None]
        if filtered_predictions:
            return filtered_predictions[0]
