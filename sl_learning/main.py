import tkinter as tk
from sl_learning.base import MenuLayout
from sl_learning.const import APPLICATION_TITLE
from sl_learning.settings.settings import settings
from sl_learning.settings.utils import autodiscover_sli


def main():
    autodiscover_sli()
    settings.load_sli()

    root = tk.Tk()
    root.title(APPLICATION_TITLE)
    root.resizable(False, False)
    main_frame = MenuLayout(root)
    main_frame.grid(row=0, column=0)
    root.eval('tk::PlaceWindow . center')
    settings.root = root
    root.mainloop()


if __name__ == "__main__":
    main()

# https://stackoverflow.com/questions/58293187/opencv-real-time-streaming-video-capture-is-slow-how-to-drop-frames-or-get-sync
# https://stackoverflow.com/questions/16366857/show-webcam-sequence-tkinter
