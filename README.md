To install the project please install all of the dependencies using Python Poetry.

To install poetry on osx, linux or bashonwindows run
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -

OR

To install python poetry please follow instruction on this link: https://python-poetry.org/docs/#installation

Disclaimer: Due to limited resources the system was tested only on 
OSx Big Sur